
/*********** Reduceres defined here *********/

import { persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage'; // default: localStorage if web, AsyncStorage if react-native
import { connectRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import encryptor from './encryptor';
import user from './modules/user';
import product from './modules/product';
export const history = createBrowserHistory();

const userPersistConfig = {
  key: 'chat-app',
  storage: storage,
  transforms: [encryptor],
  blacklist: ['router']
};

export default persistCombineReducers(userPersistConfig, {
  user,
  product,
  router: connectRouter(history)
});
