/*********** Routes for applications **************/
import React from 'react';
import { BrowserRouter as Router, Switch } from "react-router-dom";
import AppRoute from './AppRoute';
import { Auth } from '../auth';
import { publicLayout, privateLayout } from '../components/Layouts';
import Home from '../containers/home';
import NotFound from '../components/NoFound';

const Routers = store => {
    return (
        <Switch>
            <AppRoute
                exact={true}
                path="/"
                component={Home}
                requireAuth={Auth}
                layout={publicLayout}
                store={store}
                type="public"
            />

            <AppRoute
                exact
                path="*"
                component={NotFound}
                requireAuth={Auth}
                layout={publicLayout}
                store={store}
                type="public"
            />
        </Switch>
    );
};

export default Routers;