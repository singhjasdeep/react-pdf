/*
 * @file: App.js
 * @description: App Configration
 * @date: 28.11.2019
 * @author: Jasdeep Singh
 * */

import React from 'react';
import { PersistGate } from 'redux-persist/es/integration/react';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import Routers from './routers';
import { Provider } from 'react-redux';
import configureStore from './config';
import Loader from './components/Loader';
export const history = createBrowserHistory();
/************ store configration *********/
const { persistor, store } = configureStore(history);

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={<Loader />} persistor={persistor}>
        <ConnectedRouter history={history}>
          <Routers {...store} />
        </ConnectedRouter>
      </PersistGate>
    </Provider>
  );
};

export default App;
