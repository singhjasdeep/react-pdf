/*
 * @file: constants.js
 * @description: It Contain action types Related Action.
 * @author: Jasdeep Singh
 */

/*********** USER ***********/
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOG_OUT = 'LOG_OUT';
/************ LISTING *************/
export const LISTING = 'LISTING';